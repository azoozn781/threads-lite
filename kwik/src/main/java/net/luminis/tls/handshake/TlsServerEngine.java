/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.tls.handshake;

import static net.luminis.tls.CipherSuite.TLS_AES_128_GCM_SHA256;
import static net.luminis.tls.SignatureScheme.ecdsa_secp256r1_sha256;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha256;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha384;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha512;


import net.luminis.LogUtils;
import net.luminis.tls.CipherSuite;
import net.luminis.tls.HandshakeType;
import net.luminis.tls.NamedGroup;
import net.luminis.tls.ProtectionKeysType;
import net.luminis.tls.SignatureScheme;
import net.luminis.tls.TlsConstants;
import net.luminis.tls.TlsProtocolException;
import net.luminis.tls.TlsState;
import net.luminis.tls.TranscriptHash;
import net.luminis.tls.alert.DecryptErrorAlert;
import net.luminis.tls.alert.HandshakeFailureAlert;
import net.luminis.tls.alert.IllegalParameterAlert;
import net.luminis.tls.alert.MissingExtensionAlert;
import net.luminis.tls.alert.UnexpectedMessageAlert;
import net.luminis.tls.extension.ClientHelloPreSharedKeyExtension;
import net.luminis.tls.extension.Extension;
import net.luminis.tls.extension.KeyShareExtension;
import net.luminis.tls.extension.PskKeyExchangeModesExtension;
import net.luminis.tls.extension.SignatureAlgorithmsExtension;
import net.luminis.tls.extension.SupportedGroupsExtension;
import net.luminis.tls.extension.SupportedVersionsExtension;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class TlsServerEngine extends TlsEngine implements ServerMessageProcessor {
    private static final String TAG = TlsServerEngine.class.getSimpleName();
    private static final List<SignatureScheme> SUPPORTED_SIGNATURES = List.of(
            rsa_pss_rsae_sha256,
            rsa_pss_rsae_sha384,
            rsa_pss_rsae_sha512,
            ecdsa_secp256r1_sha256);
    private final List<CipherSuite> supportedCiphers = new ArrayList<>();
    private final List<X509Certificate> serverCertificateChain;
    private final PrivateKey certificatePrivateKey;
    private final TranscriptHash transcriptHash;
    private final List<Extension> serverExtensions = new ArrayList<>();
    private final List<TlsConstants.PskKeyExchangeMode> clientSupportedKeyExchangeModes;
    private final byte currentTicketNumber = 0;
    protected TlsStatusEventHandler statusHandler;
    private ServerMessageSender serverMessageSender;
    private CipherSuite selectedCipher;
    private String selectedApplicationLayerProtocol;
    private Status status = Status.Initial;


    public TlsServerEngine(List<X509Certificate> certificates, PrivateKey certificateKey, ServerMessageSender serverMessageSender, TlsStatusEventHandler tlsStatusHandler) {
        this.serverCertificateChain = certificates;
        this.certificatePrivateKey = certificateKey;
        this.serverMessageSender = serverMessageSender;
        this.statusHandler = tlsStatusHandler;
        this.transcriptHash = new TranscriptHash(32);
        this.clientSupportedKeyExchangeModes = new ArrayList<>();
        this.supportedCiphers.add(TLS_AES_128_GCM_SHA256);
    }

    public TlsServerEngine(X509Certificate serverCertificate, PrivateKey certificateKey, ServerMessageSender serverMessageSender, TlsStatusEventHandler tlsStatusHandler) {
        this(List.of(serverCertificate), certificateKey, serverMessageSender, tlsStatusHandler);
    }

    @Override
    public void received(ClientHello clientHello, ProtectionKeysType protectedBy) throws TlsProtocolException, IOException {

        status = Status.ClientHelloReceived;
        // Find first cipher that server supports
        selectedCipher = clientHello.getCipherSuites().stream()
                .filter(supportedCiphers::contains)
                .findFirst()
                // https://tools.ietf.org/html/rfc8446#section-4.1.1
                // "If the server is unable to negotiate a supported set of parameters (...) it MUST abort the handshake
                // with either a "handshake_failure" or "insufficient_security" fatal alert "
                .orElseThrow(() -> new HandshakeFailureAlert("Failed to negotiate a cipher (server only supports " + supportedCiphers.stream().map(Enum::toString).collect(Collectors.joining(", ")) + ")"));

        SupportedGroupsExtension supportedGroupsExt = (SupportedGroupsExtension) clientHello.getExtensions().stream()
                .filter(ext -> ext instanceof SupportedGroupsExtension)
                .findFirst()
                .orElseThrow(() -> new MissingExtensionAlert("supported groups extension is required in Client Hello"));



        // This implementation (yet) only supports secp256r1 and x25519(only when skd version >= 33)
        List<NamedGroup> serverSupportedGroups = List.of(NamedGroup.secp256r1, NamedGroup.x25519);

        if (supportedGroupsExt.getNamedGroups().stream()
                .noneMatch(serverSupportedGroups::contains)) {
            LogUtils.error(TAG, "Failed to negotiate supported group");
            throw new HandshakeFailureAlert(String.format("Failed to negotiate supported " +
                    "group (server only supports %s)", serverSupportedGroups));
        }

        KeyShareExtension keyShareExtension = (KeyShareExtension) clientHello.getExtensions().stream()
                .filter(ext -> ext instanceof KeyShareExtension)
                .findFirst()
                .orElseThrow(() -> new MissingExtensionAlert("key share extension is required in Client Hello"));

        KeyShareExtension.KeyShareEntry keyShareEntry = keyShareExtension.getKeyShareEntries().stream()
                .filter(entry -> serverSupportedGroups.contains(entry.getNamedGroup()))
                .findFirst()
                .orElseThrow(() -> new IllegalParameterAlert("key share named group not supported (and no HelloRetryRequest support)"));

        SignatureAlgorithmsExtension signatureAlgorithmsExtension = (SignatureAlgorithmsExtension) clientHello.getExtensions().stream()
                .filter(ext -> ext instanceof SignatureAlgorithmsExtension)
                .findFirst()
                .orElseThrow(() -> new MissingExtensionAlert("signature algorithms extension is required in Client Hello"));

        clientHello.getExtensions().stream()
                .filter(ext -> ext instanceof PskKeyExchangeModesExtension)
                .findFirst()
                .ifPresent(extension -> clientSupportedKeyExchangeModes.addAll(((PskKeyExchangeModesExtension) extension).getKeyExchangeModes()));

        // This implementation (yet) only supports rsa_pss_rsae_sha256 (non compliant, see https://tools.ietf.org/html/rfc8446#section-9.1)
        if (!signatureAlgorithmsExtension.getSignatureAlgorithms().contains(rsa_pss_rsae_sha256)) {
            LogUtils.error(TlsServerEngine.class.getSimpleName(),
                    "Failed to negotiate signature algorithm (server only supports rsa_pss_rsae_sha256");
            throw new HandshakeFailureAlert("Failed to negotiate signature algorithm (server only supports rsa_pss_rsae_sha256");
        }


        // So: ClientHello is valid and negotiation was successful, as far as this engine is concerned.
        // Use callback to let context check other prerequisites, for example appropriate ALPN extension
        statusHandler.extensionsReceived(clientHello.getExtensions());


        if (state == null) {
            state = new TlsState(transcriptHash);
        }
        transcriptHash.record(clientHello);

        generateKeys(keyShareEntry.getNamedGroup());
        state.setOwnKey(privateKey);
        state.computeEarlyTrafficSecret();
        statusHandler.earlySecretsKnown();

        List<Extension> extensions = List.of(
                new SupportedVersionsExtension(HandshakeType.server_hello),
                new KeyShareExtension(publicKey, keyShareEntry.getNamedGroup(), HandshakeType.server_hello));

        ServerHello serverHello = new ServerHello(selectedCipher, extensions);

        // Send server hello back to client
        serverMessageSender.send(serverHello);
        status = Status.ServerHelloSent;
        // Update state
        transcriptHash.record(serverHello);
        state.setPeerKey(keyShareEntry.getKey());

        // Compute keys
        state.computeSharedSecret();
        state.computeHandshakeSecrets();
        statusHandler.handshakeSecretsKnown();

        EncryptedExtensions encryptedExtensions = new EncryptedExtensions(serverExtensions);
        serverMessageSender.send(encryptedExtensions);
        transcriptHash.record(encryptedExtensions);
        status = Status.EncryptedExtensionsSent;

        SignatureAlgorithmsExtension extension = new SignatureAlgorithmsExtension(SUPPORTED_SIGNATURES);
        CertificateRequestMessage certificateRequestMessage = new CertificateRequestMessage(extension);
        serverMessageSender.send(certificateRequestMessage);
        transcriptHash.record(certificateRequestMessage);
        status = Status.CertificateRequestSent;


        CertificateMessage certificate = new CertificateMessage(serverCertificateChain);
        serverMessageSender.send(certificate);
        transcriptHash.recordServer(certificate);
        status = Status.CertificateSent;

        // "The content that is covered under the signature is the hash output as described in Section 4.4.1, namely:
        //      Transcript-Hash(Handshake Context, Certificate)
        byte[] hash = transcriptHash.getServerHash(HandshakeType.certificate);
        byte[] signature = computeSignature(hash, certificatePrivateKey, ecdsa_secp256r1_sha256, false);
        CertificateVerifyMessage certificateVerify = new CertificateVerifyMessage(ecdsa_secp256r1_sha256, signature);
        serverMessageSender.send(certificateVerify);
        transcriptHash.recordServer(certificateVerify);
        status = Status.CertificateVerifySent;


        byte[] hmac = computeFinishedVerifyData(transcriptHash.getServerHash(HandshakeType.certificate_verify), state.getServerHandshakeTrafficSecret());
        FinishedMessage finished = new FinishedMessage(hmac);
        serverMessageSender.send(finished);
        transcriptHash.recordServer(finished);
        state.computeApplicationSecrets();
        status = Status.FinishedSent;
    }

    @Override
    public void received(FinishedMessage clientFinished, ProtectionKeysType protectedBy) throws TlsProtocolException, IOException {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.CertificateVerifyReceived) {
            throw new UnexpectedMessageAlert("unexpected certificate message");
        }
        status = Status.FinishedReceived;
        transcriptHash.recordClient(clientFinished);

        // https://tools.ietf.org/html/rfc8446#section-4.4
        // "   | Mode      | Handshake Context       | Base Key                    |
        //     +-----------+-------------------------+-----------------------------+
        //     | Client    | ClientHello ... later   | client_handshake_traffic_   |
        //     |           | of server               | secret                      |
        //     |           | Finished/EndOfEarlyData |                             |
        // https://datatracker.ietf.org/doc/html/rfc8446#section-4.4.4
        // "The verify_data value is computed as follows:
        //   verify_data = HMAC(finished_key, Transcript-Hash(Handshake Context, Certificate*, CertificateVerify*))
        //      * Only included if present."
        byte[] serverHmac = computeFinishedVerifyData(transcriptHash.getClientHash(HandshakeType.certificate_verify), state.getClientHandshakeTrafficSecret());
        // https://tools.ietf.org/html/rfc8446#section-4.4
        // "Recipients of Finished messages MUST verify that the contents are correct and if incorrect MUST terminate the connection with a "decrypt_error" alert."
        if (!Arrays.equals(clientFinished.getVerifyData(), serverHmac)) {
            throw new DecryptErrorAlert("incorrect finished message");
        }

        state.computeResumptionMasterSecret();
        statusHandler.handshakeFinished();

    }

    protected boolean validateBinder(ClientHelloPreSharedKeyExtension.PskBinderEntry pskBinderEntry, int binderPosition, ClientHello clientHello) {
        // https://datatracker.ietf.org/doc/html/rfc8446#section-4.2.11, section 4.2.11.2
        byte[] partialCH = Arrays.copyOfRange(clientHello.getBytes(), 0, clientHello.getPskExtensionStartPosition() + binderPosition);
        byte[] binder = state.computePskBinder(partialCH);
        return Arrays.equals(pskBinderEntry.getHmac(), binder);
    }

    public void addSupportedCiphers(List<CipherSuite> cipherSuites) {
        supportedCiphers.addAll(cipherSuites);
    }

    public void setServerMessageSender(ServerMessageSender serverMessageSender) {
        this.serverMessageSender = serverMessageSender;
    }

    public void setStatusHandler(TlsStatusEventHandler statusHandler) {
        this.statusHandler = statusHandler;
    }

    @Override
    public CipherSuite getSelectedCipher() {
        return selectedCipher;
    }

    public List<Extension> getServerExtensions() {
        return serverExtensions;
    }

    public void addServerExtensions(Extension extension) {
        serverExtensions.add(extension);
    }

    public void setSelectedApplicationLayerProtocol(String applicationProtocol) {
        if (applicationProtocol == null) {
            throw new IllegalArgumentException();
        }
        selectedApplicationLayerProtocol = applicationProtocol;
    }


    @Override
    public void received(CertificateMessage certificateMessage, ProtectionKeysType protectedBy) throws TlsProtocolException {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }

        if (status != Status.FinishedSent) {
            // https://tools.ietf.org/html/rfc8446#section-4.4
            // "TLS generally uses a common set of messages for authentication, key confirmation, and handshake
            //   integrity: Certificate, CertificateVerify, and Finished.  (...)  These three messages are always
            //   sent as the last messages in their handshake flight."
            throw new UnexpectedMessageAlert("unexpected certificate message");
        }

        if (certificateMessage.getRequestContext().length > 0) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.2
            // "If this message is in response to a CertificateRequest, the value of certificate_request_context in that
            // message. Otherwise (in the case of server authentication), this field SHALL be zero length."
            throw new IllegalParameterAlert("certificate request context should be zero length");
        }
        if (certificateMessage.getEndEntityCertificate() == null) {
            throw new IllegalParameterAlert("missing certificate");
        }

        remoteCertificate = certificateMessage.getEndEntityCertificate();
        remoteCertificateChain = certificateMessage.getCertificateChain();
        transcriptHash.recordClient(certificateMessage);
        status = Status.CertificateReceived;
    }

    @Override
    public void received(CertificateVerifyMessage certificateVerifyMessage, ProtectionKeysType protectedBy) throws TlsProtocolException {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.CertificateReceived) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.3
            // "When sent, this message MUST appear immediately after the Certificate message and immediately prior to
            // the Finished message."
            throw new UnexpectedMessageAlert("unexpected certificate verify message");
        }

        SignatureScheme signatureScheme = certificateVerifyMessage.getSignatureScheme();
        if (!SUPPORTED_SIGNATURES.contains(signatureScheme)) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.3
            // "If the CertificateVerify message is sent by a server, the signature algorithm MUST be one offered in
            // the client's "signature_algorithms" extension"
            throw new IllegalParameterAlert("signature scheme does not match");
        }


        byte[] signature = certificateVerifyMessage.getSignature();
        if (!verifySignature(signature, signatureScheme, remoteCertificate,
                transcriptHash.getClientHash(HandshakeType.certificate), true)) {
            throw new DecryptErrorAlert("signature verification fails");
        }


        // Now the certificate signature has been validated, check the certificate validity
        checkCertificateValidity(remoteCertificateChain, false);


        transcriptHash.recordClient(certificateVerifyMessage);
        status = Status.CertificateVerifyReceived;
    }

}

