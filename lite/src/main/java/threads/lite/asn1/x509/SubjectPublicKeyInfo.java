package threads.lite.asn1.x509;

import java.util.Enumeration;

import threads.lite.asn1.ASN1BitString;
import threads.lite.asn1.ASN1Encodable;
import threads.lite.asn1.ASN1EncodableVector;
import threads.lite.asn1.ASN1Object;
import threads.lite.asn1.ASN1Primitive;
import threads.lite.asn1.ASN1Sequence;
import threads.lite.asn1.DERBitString;
import threads.lite.asn1.DERSequence;

/**
 * The object that contains the public key stored in a certificate.
 * <p>
 * The getEncoded() method in the public keys in the JCE produces a DER
 * encoded one of these.
 */
public class SubjectPublicKeyInfo extends ASN1Object {

    private final AlgorithmIdentifier algId;
    private final ASN1BitString keyData;

    private SubjectPublicKeyInfo(ASN1Sequence seq) {
        if (seq.size() != 2) {
            throw new IllegalArgumentException("Bad sequence size: "
                    + seq.size());
        }

        Enumeration<ASN1Encodable> e = seq.getObjects();

        this.algId = AlgorithmIdentifier.getInstance(e.nextElement());
        this.keyData = DERBitString.getInstance(e.nextElement());
    }

    public static SubjectPublicKeyInfo getInstance(Object obj) {
        if (obj instanceof SubjectPublicKeyInfo) {
            return (SubjectPublicKeyInfo) obj;
        } else if (obj != null) {
            return new SubjectPublicKeyInfo(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    public AlgorithmIdentifier getAlgorithm() {
        return algId;
    }


    /**
     * Produce an object suitable for an ASN1OutputStream.
     * <pre>
     * SubjectPublicKeyInfo ::= SEQUENCE {
     *                          algorithm AlgorithmIdentifier,
     *                          publicKey BIT STRING }
     * </pre>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector(2);

        v.add(algId);
        v.add(keyData);

        return new DERSequence(v);
    }
}
