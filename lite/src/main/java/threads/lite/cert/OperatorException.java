package threads.lite.cert;

public class OperatorException
        extends Exception {
    private final Throwable cause;

    public OperatorException(String msg, Throwable cause) {
        super(msg);

        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}
