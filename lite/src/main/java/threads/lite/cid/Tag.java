package threads.lite.cid;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Objects;


public interface Tag {


    class InetAddress implements Tag {
        @NonNull
        private final byte[] address;

        public InetAddress(@NonNull byte[] address) {
            this.address = address;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            InetAddress that = (InetAddress) o;
            return Arrays.equals(address, that.address);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(address);
        }

        @NonNull
        @Override
        public String toString() {
            try {
                return Objects.requireNonNull(java.net.InetAddress.getByAddress(address).getHostAddress());
            } catch (Throwable throwable) {
                throw new IllegalStateException(throwable);
            }
        }

        @NonNull
        public byte[] getAddress() {
            return address;
        }
    }

    class Address implements Tag {
        @NonNull
        private final String address;

        public Address(@NonNull String address) {
            this.address = address;
        }

        @NonNull
        public String getAddress() {
            return address;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Address address1 = (Address) o;
            return address.equals(address1.address);
        }

        @Override
        public int hashCode() {
            return Objects.hash(address);
        }

        @Override
        @NonNull
        public String toString() {
            return address;
        }
    }

    class Port implements Tag {
        private final int port;

        public Port(int port) {
            if (port > 65535) throw new IllegalStateException("Invalid port");
            this.port = port;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Port port1 = (Port) o;
            return port == port1.port;
        }

        @Override
        public int hashCode() {
            return Objects.hash(port);
        }

        @Override
        @NonNull
        public String toString() {
            return String.valueOf(port);
        }

        public int getPort() {
            return port;
        }
    }
}
