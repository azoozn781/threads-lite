package threads.lite.core;

import net.luminis.quic.TransportParameters;

import threads.lite.IPFS;

public class Parameters extends TransportParameters {

    private final boolean keepAlive;

    private Parameters(int maxIdleTimeoutInSeconds, boolean keepAlive) {
        super(maxIdleTimeoutInSeconds, IPFS.MESSAGE_SIZE_MAX,
                IPFS.MAX_STREAMS, 0);
        this.keepAlive = keepAlive;

    }

    public static Parameters getDefault() {
        return new Parameters(IPFS.GRACE_PERIOD, false);
    }

    public static Parameters getDefault(int maxIdleTimeoutInSeconds, boolean keepAlive) {
        return new Parameters(maxIdleTimeoutInSeconds, keepAlive);
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

}
